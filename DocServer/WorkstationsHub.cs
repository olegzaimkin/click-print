using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace DocServer
{
	public struct PrintJobInfo
	{
		public string FileName {get; set;}
		public string Url {get; set;}
	}
	public interface IPrintServer
	{
		Task PrintFile(string name, string url);
	}

	public class WorkstationsHub : Hub<IPrintServer>
	{
		private readonly ILogger<WorkstationsHub> _logger;
		private readonly ClientRegistry _clients;

		public WorkstationsHub(ILogger<WorkstationsHub> logger, ClientRegistry clients)
		{
			_logger = logger;
			_clients = clients;
		}

		public override Task OnConnectedAsync()
		{
			_logger.LogInformation($"New connection: {Context.ConnectionId}");

			_clients.RegisterClient(new ClientInfo(Context.ConnectionId, "untitled"));
			return base.OnConnectedAsync();
		}
		public override Task OnDisconnectedAsync(Exception exception)
		{
			_logger.LogInformation($"Disconnected: {Context.ConnectionId}");
			
			_clients.UnregisterClient(Context.ConnectionId);
			return base.OnDisconnectedAsync(exception);
		}
		public void Register(string clientName)
		{
			_clients.RegisterClient(new ClientInfo(Context.ConnectionId, clientName));
			_logger.LogInformation($"new client registered as {clientName}. {Context.ConnectionId}");
		}
	}
}