﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;

namespace DocServer.Controllers
{
	public class PrintController : Controller
	{
		private readonly IHostEnvironment _host;
		private readonly IHubContext<WorkstationsHub, IPrintServer> _hubContext;
		private readonly ClientRegistry _clients;

		public PrintController(IHostEnvironment host, IHubContext<WorkstationsHub, IPrintServer> hubContext, ClientRegistry clients)
		{
			_host = host;
			_hubContext = hubContext;
			_clients = clients;
		}

		[HttpGet]
		public IActionResult Index()
		{
			return View();
		}

		// GET
		
		[HttpPost]  
		public async Task<ActionResult> Index(IFormFile file, string client)  
		{
			if(file == null || file.Length == 0)
			{
				ViewBag.Message = "You have not specified a file.";
				return View();
			}

			if(string.IsNullOrWhiteSpace(client) || !_clients.TryGetByConnectionId(client, out var clientInfo))
			{
				ViewBag.Message = "You have not specified a client or such client is not connected.";
				return View();
			}

			if (file != null && file.Length > 0)
				try
				{
					var dirPath = Path.Combine(_host.ContentRootPath, "uploads");
					Directory.CreateDirectory(dirPath);
					var fileName = Path.ChangeExtension(Path.GetRandomFileName(), "pdf");
					var filePath = Path.Combine(dirPath, fileName);

					using(var stream = new FileStream(filePath, FileMode.Create))
					{
						file.CopyTo(stream);
						stream.Close();
					}
					await Task.Delay(10);	// some retension for FS to see the file

					var url = $"http://localhost:5000/api/documents/{fileName}";
					await _hubContext.Clients.Client(client).PrintFile(file.FileName, url);

					ViewBag.Message = $"File printed successfully to {clientInfo.ClientName}";
					ViewBag.client = client;
				}
				catch (Exception ex)
				{
					ViewBag.Message = "ERROR:" + ex.Message.ToString();
				}
			else
			{
				ViewBag.Message = "You have not specified a file.";
			}

			return View();
		}
	}
}