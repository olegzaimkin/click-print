using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;

public interface IRepository {};

namespace server
{
	[ApiController]
	[Route("api/[controller]")]
	public class DocumentsController : ControllerBase
	{
		private readonly IHostEnvironment _host;

		public DocumentsController(IHostEnvironment host)
		{
			_host = host;
		}

		[HttpGet("{fileId}")]
		public IActionResult GetFile(string fileId)
		{
			var dirPath = Path.Combine(_host.ContentRootPath, "uploads");
			var filePath = Path.Combine(dirPath, Path.GetFileName(fileId));

			return PhysicalFile(filePath, "application/octet-stream", true);
		}
	}
}