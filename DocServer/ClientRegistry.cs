using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

using System.Collections.Concurrent;

namespace DocServer
{
	public class ClientInfo
	{
		public ClientInfo(string connectionId, string clientName)
		{
			ConnectionId = connectionId;
			ClientName = clientName;
		}
		public string ConnectionId { get; private set; }
		public string ClientName { get; private set; }
	}

	/// Service for storing client list.
	public class ClientRegistry
	{
		private readonly ConcurrentDictionary<string, ClientInfo> _registry = new ConcurrentDictionary<string, ClientInfo>();

		public void RegisterClient(ClientInfo info)
		{
			_registry.AddOrUpdate(info.ConnectionId, info, (_,__) => info);
		}
		public void UnregisterClient(string connectionId)
		{
			_registry.TryRemove(connectionId, out _);
		}

		public bool TryGetByConnectionId(string connectionId, out ClientInfo clientInfo) => _registry.TryGetValue(connectionId, out clientInfo);
	}
}
