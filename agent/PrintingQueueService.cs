﻿using System.Collections.Concurrent;

namespace agent
{
    public struct Job
    {
        public string name;
        public string url;
    }

    public interface IPrintingQueue
    {
        void Enqueue(string name, string url);
        public Job? Dequeue();
    }

    public class PrintingQueueService: IPrintingQueue
    {
        private readonly ConcurrentQueue<Job> _printQueue = new ConcurrentQueue<Job>();
        
        public void Enqueue(string name, string url)
        {
            _printQueue.Enqueue(new Job { name = name, url = url });
        }

        public Job? Dequeue()
        {
            if (_printQueue.TryDequeue(out var job)) return job;
            return null;
        }
    }
}