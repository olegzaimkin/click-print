﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace agent
{
	public class PrintingService: IHostedService
	{
		private readonly IOptions<AppConfig> _config;
		private readonly IPrintingQueue _printingQueue;
		private readonly ILogger _logger;
		private readonly CancellationTokenSource _cts;

		public PrintingService(IPrintingQueue printingQueue, ILogger<PrintingService> logger, IOptions<AppConfig> config)
		{
			_config = config;
			_printingQueue = printingQueue;
			_logger = logger;
			_cts = new CancellationTokenSource();
		}

		private void PrintPdfFile(string pdfFileName)
		{
			var processStartInfo = new ProcessStartInfo
			{
				FileName = _config.Value.acrord32Path,
				WindowStyle = ProcessWindowStyle.Hidden,
				UseShellExecute = false,
				CreateNoWindow = true,
				Arguments = 
					string.IsNullOrEmpty(_config.Value.printerName) ? $"/p /h {pdfFileName}"
						: $"/h /t {pdfFileName} \"{_config.Value.printerName}\" \"\" \"\""
						// passing empty string to a port name, see https://www.adobe.com/content/dam/acom/en/devnet/acrobat/pdfs/pdf_open_parameters.pdf
			};
			var process = new Process { StartInfo = processStartInfo };
			process.Start();

			process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
			if (process.HasExited == false)
				process.WaitForExit(10000);

			process.EnableRaisingEvents = true;
			// calling a Kill() on the process is not good as it could still be printing the file.
			process.Close();
	  }
		private static bool KillAdobeRdProcesses()
		{
			foreach (var p in from process in Process.GetProcesses()
				where process.ProcessName.StartsWith("AcroRd32")
				select process)
			{
				p.Kill();
				return true;
			}
			return false;
		}

		private async Task ProcessQueue(CancellationToken cancellationToken)
		{
			var removalQueue = new Queue<string>();
			var webClient = new System.Net.WebClient();

			void Cleanup()
			{
				while(removalQueue.Any())
				{
					var removeFileName = removalQueue.Dequeue();
					try
					{
						if(File.Exists(removeFileName)) File.Delete(removeFileName);
					}
					catch(Exception e)
					{
						_logger.LogError($"Failed to remove temp file {removeFileName} due to {e.Message}");
					}
				}
			}

			while(!cancellationToken.IsCancellationRequested)
			{
				var job = _printingQueue.Dequeue();
				if(job == null)
				{
					await Task.Delay(10);
					continue;
				}
				_logger.LogInformation($"Printing {job.Value.name} from {job.Value.url}");
				Cleanup();

				var fileName = Path.ChangeExtension(Path.GetTempFileName(), "pdf");
				removalQueue.Enqueue(fileName);
				try
				{
					webClient.DownloadFile(new Uri(job.Value.url), fileName);
					_logger.LogInformation($"Job downloaded as {fileName}");

					PrintPdfFile(fileName);
					_logger.LogInformation($"Successfully printed {job.Value.url}");
				}
				catch(Exception e)
				{
					_logger.LogError($"Failed to print file: {e.ToString()}");
				}
			}
			_logger.LogInformation("Terminating printing loop.");
			Cleanup();
		}

		public Task StartAsync(CancellationToken cancellationToken)
		{
			ProcessQueue(_cts.Token);
			return Task.CompletedTask;
		}

		public async Task StopAsync(CancellationToken cancellationToken)
		{
			_cts.Cancel();
			await Task.Delay(10);
		}
	}
}