﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace agent
{
	public class ConnectionHubService : IHostedService
	{
		private HubConnection _hub;
		private readonly IOptions<AppConfig> _config;
		private readonly IPrintingQueue _printingQueue;
		private readonly ILogger _logger;

		public ConnectionHubService(IPrintingQueue printingQueue, IOptions<AppConfig> config,
			ILogger<ConnectionHubService> logger)
		{
			_printingQueue = printingQueue;
			_config = config;
			_logger = logger;
		}

		public async Task StartAsync(CancellationToken cancellationToken)
		{
			var serviceUrl = new Uri(new Uri(_config.Value.serverUrl), "/wshub");

			_hub = new HubConnectionBuilder()
				.WithUrl(serviceUrl)
				.Build();

			_hub.On<string, string>("PrintFile", _printingQueue.Enqueue);
			_logger.LogInformation("Starting connection...");

			while (!cancellationToken.IsCancellationRequested)
			{
				try
				{
					var clientName = _config.Value.clientName ?? "digital-resistance";

					await _hub.StartAsync(cancellationToken);
					await _hub.SendAsync("Register", clientName);
					_logger.LogInformation($"Connected as {clientName} (connection id is {_hub.ConnectionId})");
					
					break;
				}
				catch (Exception e)
				{
					_logger.LogError("There was an error opening the connection: {0}", e.GetBaseException());
					await Task.Delay(2000, cancellationToken);
				}
			}
		}
 
		public async Task StopAsync(CancellationToken cancellationToken)
		{
			_logger.LogInformation("Stopping...");
			await _hub.StopAsync(cancellationToken);
			await _hub.DisposeAsync();
			_hub = null;
		}
	}
}