using System;

using Microsoft.AspNetCore.SignalR.Client;
using System.Collections.Concurrent;
using System.IO;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace agent
{

	public class AppConfig
	{
		public string serverUrl { get; set; }
		public string acrord32Path { get; set; }
		public string printerName { get; set; }

		public string clientName { get; set; }
	}
	public class Program
	{
		private const string _prefix = "CLICKPRINT_";
		
		public static async Task Main(string[] args)
		{
			var host = new HostBuilder()
				.ConfigureHostConfiguration(configHost =>
				{
					configHost.SetBasePath(Directory.GetCurrentDirectory());
					configHost.AddEnvironmentVariables(prefix: _prefix);
					configHost.AddCommandLine(args);
				})
				.ConfigureAppConfiguration((hostContext, configApp) =>
				{
					configApp.SetBasePath(Directory.GetCurrentDirectory());
					configApp.AddJsonFile("appsettings.json", optional: true);
					configApp.AddJsonFile(
						$"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json",
						optional: true);
					configApp.AddEnvironmentVariables(prefix: _prefix);
					configApp.AddCommandLine(args);
				})
				.ConfigureServices((hostContext, services) =>
				{
					services.AddLogging();
					services.Configure<AppConfig>(hostContext.Configuration.GetSection("application"));
					services.AddSingleton<IPrintingQueue, PrintingQueueService>();
					services.AddHostedService<PrintingService>();
					services.AddHostedService<ConnectionHubService>();
 
				})
				.ConfigureLogging((hostContext, configLogging) =>
				{
					configLogging.AddConsole();
 
				})
				.UseConsoleLifetime()
				.Build();
 
			await host.RunAsync();

		}
	}
}
