## About

Goal of the project is how to print to a local printer from a web page hosting any of ours Javascript viewers. This applies to JsViewer of AR15, viewer in the Wyn Reports, ARJS Viewer component and so on.

Browser API does not allow accessing the printer by a security reasons. Our customers used to print via ActiveX viewer control and it worked in the very early IE versions as the IE was a leaky sieve at those times.

The solution implemented here is to run "agent" application on the client side and communicate it either directly from the client browser (this is very inlikely allowed by a CORS policy of the modern browsers) or from a server hosting customer application.

## Printing server

The server server printing requests and route the documents to respective client PC.

Run the server using the following command line:
```cmd
cd DocServer
dotnet run
```

That starts the server that than could be found on "http://localhost:5000" (ok to configure).

## Running client-side printing agent

That process is supposed to run on the client PC in the background.
Just run it:

```cmd
cd agent
dotnet run
```

Default printing address could be found in the `appsettings.json` or you could override it via command line like that:

```cmd
dotnet run -- application:serverUrl=http://corpserver.local"
```

## Demo scenario

Find the client id in the "doc server" console logs.
Navigate to a server's Print page, choose the document and paste client id to a respective fields then click "Print file".

## Research notes (post-mortem)

Technically click-print is rather simplistic while there're many technical obtables in various places.

### Security considerations

The solution should effectively prevent system from DDoS by sending printer jobs from anuthorized applications, other kind of intruders.

### Resilient communication with a server

It has to maintain communication to a server, display connection status in the tray, automativaly resume the network connection whenever server is available.
The code in the sample uses SignalR to communicate in two directions. That seems to be overkill, and it also bring weakness. I suppose it should be implemented via regular long poll requests + ping-pong-alike lifecycle control messages.

### Document exchange format

The pdf was choosen in this sample as common denominator for all projects (not limited to AR).
We could consider using AR snapshots as well but that would limit the solution to AR Desktop.

Drawbacks - rather slow.

### Printing on the agent

Current solution uses Adobe Reader to be print the documents and relies on its Command-Line Interface parameters. Unfortunately it's hard or impossible to control application lifecycle and particularly caller doesn't know if the file is sent to printer or not.

We should consider using on of free Pdf libraries and one of them is Pdfium by google or our own PdfDocs (if any).

### Application and agent handshaking protocol

In current implementation it's assumed that agent has a "name" which is defined in the config/parameters and server can communicate to a client by that name. The user scenario could be:

* agent provides a UI "connect to server" and provides a "token" (could be a short number, say 5 digits)
* server application provides "connect local printer" UI which allows to enter token to complete the setup

There could be some variations but none of them could provide automatic handshaking by a nature of the web and security basics.

### Installing the agent

We need a simple click-once installer, and it should not require full .net framework to be installed (should be ok to rely on 3.5 fwk available out of the box on windows 10).

The installer could contain the connection url to the server. In case agent is already installed it should just add the new server config to existing agent installation.

Agent should provide "Update" function in case we deploy a new binary. It should be absolutely backward compatible.